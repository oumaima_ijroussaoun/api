from sqlalchemy.orm import Session, query, session 
from sqlalchemy import column, func 
import pandas as pd
from sqlalchemy import desc
import json
import locale
from typing import List
from sqlalchemy.sql.expression import  case, or_, true,and_
from sqlalchemy.sql.sqltypes import String
import requests
from sqlalchemy import func
from typing import Dict
import pandas as pd
from pyhocon import ConfigFactory
import os
import time
import sqlalchemy as sql
import xml.etree.ElementTree as ET
import time
import re
import itertools
from sqlalchemy import create_engine
import paramiko
import io
from lxml import etree
from loguru import logger
from datetime import date , datetime, timedelta
import psycopg2
import numpy as np
from fastapi.encoders import jsonable_encoder
conf=ConfigFactory.parse_file('PAN_PDS_API/crud/conf.conf')
path_dict=conf.get('path_dict')

############################################################# Connexion à la BDD ##############################################################################
def create_connection (connection_string):
    print("-- Connecting to database")
    try:
        
        sql_engine = sql.create_engine(connection_string)
        print("Connected to database successfully !")
        return sql_engine

    except Exception as e:
        print("Connection to database failed, check the database status as well as your network configuration")
        print (e)

db_postgre="postgresql://postgres:Hive2021@192.168.55.6:5432/panda_2"
con=create_connection(db_postgre)


############################################################ Fonctions auxiliaires #############################################################################""

def filtrer_dataframe_advanced(df, **args):
    filter_series = pd.Series(True, index=df.index)
    for key, value in args.items():
        if isinstance(value, dict):
            op = value.get('op', 'eq')  
            value = value.get('value', None) 
            if op == 'eq':
                filter_series &= (df[key] == value)
            elif op == 'ne':
                filter_series &= (df[key] != value)
            elif op == 'lt':
                filter_series &= (df[key] < value)
            elif op == 'le':
                filter_series &= (df[key] <= value)
            elif op == 'gt':
                filter_series &= (df[key] > value)
            elif op == 'ge':
                filter_series &= (df[key] >= value)
        else:
            filter_series &= df[key].isin(value)
    return df.loc[filter_series]



############################################################ Foyers Alarmes #############################################################################""

def get_foyer_alarmes(date_debut,date_fin):
    Nmspce={'mcm':'http://www.mediametrie.fr/nge/schemas'}
    parser = etree.XMLParser(remove_blank_text=True)
    #date = datetime.strptime(date_debut, '%Y%m%d')
    tree = etree.parse(f'PAN_PDS_API/input/aud/aud_{date_debut.strftime("%Y%m%d")}.xml',parser)
    # tree = etree.parse(f'./input/aud_{date}.xml',parser)
    root = tree.findall(".//mcm:foyer",namespaces=Nmspce)
    df_rows = []

    for foyer in root:
        traitement_foyer = foyer.find('.//mcm:traitement_foyer', namespaces=Nmspce)

        for alarme in traitement_foyer.findall('.//mcm:alarme', namespaces=Nmspce):
            params = []

            for param in alarme.findall('.//mcm:parametre', namespaces=Nmspce):
                
                
                params.append({param.get('nom'):param.get('valeur')}) 
                

            
            df_rows.append([
                date_debut,
                foyer.get('foyer_id'),
                traitement_foyer.get('resultat'),
                traitement_foyer.get('vacancier'),
                alarme.get('id'),
                alarme.get('id_regle'),
                params if len(params)!=0 else None
            ])

    alarme_df = pd.DataFrame(df_rows, columns=['date', 'id_foy', 'foyer_etat', 'vacc', 'id_alarme', 'id_regle', 'params'])

    # Drop columns with all NaN values
    alarme_df = alarme_df.dropna(how='all', axis=1)

    # Rename columns in place
    alarme_df.rename(columns={
        'date': 'Date',
        'id_foy': 'Foyer',
        'foyer_etat': 'Etat Foyer',
        'vacc': 'Vaccancier',
        'id_alarme': 'Id alarme',
        'id_regle': 'Id règle',
        'params': 'Paramètres'
    }, inplace=True)

    print(alarme_df)
    date = date_debut.strftime("%Y%m%d")
    alarme_df['Date']=date
    return alarme_df.to_dict("records")

    #return alarme_df.to_dict("records")
   


###################################################################### Alarmes des meters #################################################################################
def get_meter_alarmes(date_debut,date_fin):
    Nmspce={'mcm':'http://www.mediametrie.fr/nge/schemas'}
    parser = etree.XMLParser(remove_blank_text=True)
    #date = datetime.strptime(date_debut, '%Y%m%d')
    tree = etree.parse(f'PAN_PDS_API/input/aud/aud_{date_debut.strftime("%Y%m%d")}.xml',parser)
    # tree = etree.parse(f'./input/aud_{date}.xml',parser)
    root = tree.findall(".//mcm:foyer",namespaces=Nmspce)
    df_rows = []

    for foyer in root:
        traitement_meter = foyer.find('.//mcm:traitement_meter', namespaces=Nmspce)

        for alarme in traitement_meter.findall('.//mcm:alarme', namespaces=Nmspce):
            params = []

            for param in alarme.findall('.//mcm:parametre', namespaces=Nmspce):
                
                
                params.append({param.get('nom'):param.get('valeur')}) 
                

            
            df_rows.append([
                date_debut,
                foyer.get('foyer_id'),
                traitement_meter.get('resultat'),
                alarme.get('id'),
                alarme.get('id_regle'),
                params if len(params)!=0 else None
            ])

    alarme_df = pd.DataFrame(df_rows, columns=['date', 'id_foy', 'résultat_meter', 'id_alarme', 'id_regle', 'params'])

    # Drop columns with all NaN values
    alarme_df = alarme_df.dropna(how='all', axis=1)

    # Rename columns in place
    alarme_df.rename(columns={
        'date': 'Date',
        'id_foy': 'Foyer',
        'résultat_meter': 'Résultat Meter',
        'id_alarme': 'Id alarme',
        'id_regle': 'Id règle',
        'params': 'Paramètres'
    }, inplace=True)

    print(alarme_df)
    date = date_debut.strftime("%Y%m%d")
    alarme_df['Date']=date
    return alarme_df.to_dict("records")

###################################################################### Audience  #################################################################################

def aud_xml(date_debut,param):

    Nmspce = {'mcm': 'http://www.mediametrie.fr/nge/schemas'}
    parser = etree.XMLParser(remove_blank_text=True)
    tree = etree.parse(f'PAN_PDS_API/input/aud/aud_{date_debut.strftime("%Y%m%d")}.xml',parser)
    #tree = etree.parse('/home/issam/Desktop/Analyse_PAN_PDS/PAN_PDS_API/input/aud', f'aud_{date_deb}.xml', parser)

    root = tree.findall(".//mcm:foyer", namespaces=Nmspce)
    
    if(param=='foyer'):
        
        return "no audience"
        
    else:
        df_rows=[]
        for foyer in root :
            inv=0
            for elt in foyer.findall(path_dict[f"{param}_path"],namespaces=Nmspce):
                inv+=1
                traitement=elt.find(path_dict[f"trait_{param}"],namespaces=Nmspce)
                if (not(len(traitement.xpath('.//mcm:ticket',namespaces=Nmspce)))):
                        if (param=='inv_occ'):
                            df_rows.append([date,foyer.get('foyer_id'),inv,elt.get('sexe'),elt.get('age'),traitement.get('resultat')\
                                         ,traitement.get('vacancier'),None\
                                         ,None,None,None,None])
                        else:
                            df_rows.append([date,foyer.get('foyer_id'),elt.get(path_dict[f"id_{param}"]),elt.get('sexe'),elt.get('age'),traitement.get('resultat')\
                                         ,traitement.get('vacancier'),None\
                                         ,None,None,None,None])
                for audience in traitement.findall(path_dict[f"aud_{param}"],namespaces=Nmspce):

                        for ticket in audience.findall('.//mcm:ticket',namespaces=Nmspce):
                            if (param=='inv_occ'):
                                df_rows.append([date,foyer.get('foyer_id'),inv,elt.get('sexe'),elt.get('age'),traitement.get('resultat')\
                                     ,traitement.get('vacancier'),ticket.get('debut')\
                                     ,ticket.get('fin'),int(ticket.get('fin'))-int(ticket.get('debut')),audience.get('id'),ticket.get('chaine')])
                            else:
                                df_rows.append([date,foyer.get('foyer_id'),elt.get(path_dict[f"id_{param}"]),elt.get('sexe'),elt.get('age'),traitement.get('resultat')\
                                     ,traitement.get('vacancier'),ticket.get('debut')\
                                     ,ticket.get('fin'),int(ticket.get('fin'))-int(ticket.get('debut')),audience.get('id'),ticket.get('chaine')])

        final=pd.DataFrame(df_rows, columns=["date","id_foy",f"id_{param}","sexe_inv","age_inv", \
                                              f"{param}_etat","vacc","deb","fin","durree","poste","id_chaine"]).sort_values(by=["date",'id_foy', f"id_{param}"]).dropna(how='all', axis=1).fillna(0)
        
        types={"id_foy":int,f"id_{param}":int,"deb":int,"fin":int,"id_foy":int,'poste':int,'id_chaine':int,\
                            'durree':int,f"{param}_etat":str,"vacc":str,"sexe_inv":str,"age_inv":str,'date':str}
        
        columns_types={key: types[key] for key in final.columns.tolist()}
        
        final=final.astype(columns_types)
        final['date']=date_debut
        
        return final.to_dict("records")
