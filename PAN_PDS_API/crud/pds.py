from datetime import datetime ,date
import datetime
from datetime import datetime
from sqlalchemy.orm import Session, query, session 
from sqlalchemy import column, func 
import pandas as pd
from sqlalchemy import desc
import json
import locale
from typing import List
from sqlalchemy.sql.expression import  case, or_, true,and_
from sqlalchemy.sql.sqltypes import String
import requests
from sqlalchemy import func
from typing import Dict
import pandas as pd
from pyhocon import ConfigFactory
import time
from datetime import datetime,timedelta
import os
import pandas as pd
import time
from loguru import logger
import sqlalchemy as sql
import json
import psycopg2
import paramiko
import numpy as np
    


############################################################# Connexion à la BDD ##############################################################################
def create_connection (connection_string):
    print("-- Connecting to database")
    try:
        
        sql_engine = sql.create_engine(connection_string)
        print("Connected to database successfully !")
        return sql_engine

    except Exception as e:
        print("Connection to database failed, check the database status as well as your network configuration")
        print (e)

db_postgre="postgresql://postgres:Hive2021@192.168.55.6:5432/panda_2"
con=create_connection(db_postgre)


############################################################ Fonctions auxiliaires #############################################################################""

def filtrer_dataframe_advanced(df, **args):
    filter_series = pd.Series(True, index=df.index)
    for key, value in args.items():
        if isinstance(value, dict):
            op = value.get('op', 'eq')  
            value = value.get('value', None) 
            if op == 'eq':
                filter_series &= (df[key] == value)
            elif op == 'ne':
                filter_series &= (df[key] != value)
            elif op == 'lt':
                filter_series &= (df[key] < value)
            elif op == 'le':
                filter_series &= (df[key] <= value)
            elif op == 'gt':
                filter_series &= (df[key] > value)
            elif op == 'ge':
                filter_series &= (df[key] >= value)
        else:
            filter_series &= df[key].isin(value)
    return df.loc[filter_series]



def format_heure(valeur):
    heures = valeur // 3600
    minutes = (valeur % 3600) // 60
    secondes = valeur % 60
    return "{:02d}:{:02d}:{:02d}".format(heures, minutes, secondes)

def get_foyer(valeur):
    return valeur[0:4]


def get_touche(valeur):
    return valeur[5:7]


def get_foyer_total_membre(valeur):
    effectif_foyer = pd.read_sql(
    "SELECT  foyer.f_num_con,effectif_foyer.total_membre "
    "FROM foyer "
    "JOIN effectif_foyer ON foyer.f_num_con = effectif_foyer.f_num_con "
    "WHERE foyer.f_id_mediamat = %s",
    con,
    params=(valeur,)
    )   
    effectif_foyer=pd.DataFrame(effectif_foyer)
    return effectif_foyer


def get_total_membre(row):
        return get_foyer_total_membre(row['Foyer'])['total_membre']
    
def get_Num_Con(row):
        return get_foyer_total_membre(row['Foyer'])['f_num_con']



def get_id_individus(valeur1,valeur2):
    ids = pd.read_sql(
    "SELECT  id_indiv "
    "FROM individu "
    "WHERE f_num_con = %s and touche_indiv = %s",
    con,
    params=(valeur1,valeur2)
    )   
    ids=pd.DataFrame(ids)
    return ids



def get_id_indiv(row):
    return get_id_individus(row['f_num_con'],row['Touche'])['id_indiv']



############################################################### Foyers entre dates ####################################################################################
def get_foyers_pds_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')

    infosFoyer = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/2022', f'{date.strftime("%Y%m%d")}.pds')
        print(chemin_fichier)
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] == "00":
                    ligne = ligne.strip()
                    foyer_infos = {
                        "Date": date.strftime('%Y-%m-%d'),
                        "Type": "H",
                        "N_invité": 0,
                        "sexe": "0",
                        "Tranche_age": "0",
                        "Heure_début": 0,
                        "Heure_Fin": 0,
                        "Chaîne": 0,
                        "Indiv": ligne[1:7],
                        "poids": ligne[8:12]
                    }
                    infosFoyer.append(foyer_infos)

    df_foyers = pd.DataFrame(infosFoyer)

    df_foyers['Date'] = pd.to_datetime(df_foyers['Date'])

    df_foyers['Date'] = df_foyers['Date'].dt.strftime('%Y%m%d')


    df_foyers = df_foyers[['Date', 'Indiv', 'Type', 'N_invité', 'sexe', 'Tranche_age', 'poids', 'Heure_début', 'Heure_Fin', 'Chaîne']]
    df_foyers.rename(columns={'poids': 'poids', 'Indiv': 'Indiv'}, inplace=True)
    df_foyers=df_foyers.to_dict("records")
    return df_foyers


############################################################################ Individus entre dates #####################################################################
def individus_pds_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosIndividus = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/2022', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] in ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]:
                    contenu_ligne = ligne[13:]
                    contenu_ligne=contenu_ligne.split(',')[:-1]
                    if len(contenu_ligne)!= 0 :
                        for i in contenu_ligne:
                            ligne_infos = {}  
                            ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                            ligne_infos["Type"]="I"
                            ligne_infos["N_invité"]=0
                            ligne_infos["sexe"]="0"
                            ligne_infos["Tranche_age"]="0"
                            ligne_infos["Indiv"] = ligne[1:7]
                            ligne_infos["poids"] = ligne[8:12]
                            ligne_infos["Heure_début"] = int(i[1:6])
                            ligne_infos["Heure_Fin"] = int(i[6:11])
                            ligne_infos["Chaîne"] = i[12:15]
                            infosIndividus.append(ligne_infos)
                    else:
                        ligne_infos = {}  
                        ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                        ligne_infos["Type"]="I"
                        ligne_infos["N_invité"]=0
                        ligne_infos["sexe"]="0"
                        ligne_infos["Tranche_age"]="0"
                        ligne_infos["Indiv"] = ligne[1:7]
                        ligne_infos["poids"] = ligne[8:12]
                        ligne_infos["Heure_début"] = 0
                        ligne_infos["Heure_Fin"] = 0
                        ligne_infos["Chaîne"] = 0
                        infosIndividus.append(ligne_infos)               
    df_individus = pd.DataFrame(infosIndividus)

    df_individus['Date'] = pd.to_datetime(df_individus['Date'])

    df_individus['Date'] = df_individus['Date'].dt.strftime('%Y%m%d')


    new_order = ['Date', 'Indiv', 'Type','N_invité','sexe','Tranche_age','poids','Heure_début','Heure_Fin','Chaîne']
    df_individus=df_individus[new_order]
    df_individus=df_individus.to_dict("records")
    return df_individus

############################################################################## Invites entre dates #######################################################################
def invites_pds_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosInvites = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/2022', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue
        foyers_invites = {}
        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] == '32':
                    foyer_id = ligne[1:5]
                    sessions = ligne[17:]
                    sessions = sessions.split(',')[:-1]
                    if foyer_id in foyers_invites:
                        id_inv = foyers_invites[foyer_id] + 1
                        foyers_invites[foyer_id] = id_inv
                    else:
                        id_inv = 1
                        foyers_invites[foyer_id] = id_inv
                    if len(sessions)!= 0 :
                        for i in sessions:
                            invites_infos = {'N_invité': id_inv}
                            invites_infos["Date"]=date
                            invites_infos["Type"]="G"
                            invites_infos["Indiv"] = ligne[1:7]
                            invites_infos["poids"] = ligne[8:12]
                            invites_infos["sexe"] = ligne[13:14]
                            invites_infos["Tranche_age"] = ligne[15:16]
                            invites_infos["Heure_début"] = int(i[1:6])
                            invites_infos["Heure_Fin"] = int(i[6:11])
                            invites_infos["Chaîne"] = i[12:15]
                            infosInvites.append(invites_infos)
                    else:
                            invites_infos = {'N_invité': id_inv}
                            invites_infos["Date"]=date
                            invites_infos["Type"]="G"
                            invites_infos["Indiv"] = ligne[1:7]
                            invites_infos["poids"] = ligne[8:12]
                            invites_infos["sexe"] = ligne[13:14]
                            invites_infos["Tranche_age"] = ligne[15:16]
                            invites_infos["Heure_début"] = 0
                            invites_infos["Heure_Fin"] = 0
                            invites_infos["Chaîne"] = 0
                            infosInvites.append(invites_infos)
                        
    df_invites = pd.DataFrame(infosInvites)
    df_invites['Date'] = pd.to_datetime(df_invites['Date'])

    df_invites['Date'] = df_invites['Date'].dt.strftime('%Y%m%d')

    new_order = ['Date', 'Indiv', 'Type','N_invité','sexe','Tranche_age','poids','Heure_début','Heure_Fin','Chaîne']
    df_invites=df_invites[new_order]
    df_invites=df_invites.to_dict('records')
    return df_invites


############################################################################ Postes entre dates #########################################################################

def postes_pds_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosPostes = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/2022', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] in ["61", "62"]:
                    contenu_ligne = ligne[13:]
                    contenu_ligne=contenu_ligne.split(',')[:-1]
                    if len(contenu_ligne)!= 0 :
                        for i in contenu_ligne:
                            ligne_infos = {}  
                            ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                            ligne_infos["Poste"]=ligne[5:7]
                            ligne_infos["Foyer"] = ligne[1:5]
                            ligne_infos["poids"] = ligne[8:12]
                            ligne_infos["Heure_début"] = int(i[1:6])
                            ligne_infos["Heure_Fin"] = int(i[6:11])
                            ligne_infos["Num_poste"] = int(i[11:12])
                            ligne_infos["Chaîne"] = i[12:15]
                            infosPostes.append(ligne_infos)
                    else:
                        ligne_infos = {}  
                        ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                        ligne_infos["Poste"]=ligne[5:7]
                        ligne_infos["Foyer"] = ligne[1:5]
                        ligne_infos["poids"] = ligne[8:12]
                        ligne_infos["Heure_début"] = 0
                        ligne_infos["Heure_Fin"] = 0
                        ligne_infos["Num_poste"] = 0
                        ligne_infos["Chaîne"] = 0
                        infosPostes.append(ligne_infos)               
    df_postes = pd.DataFrame(infosPostes)
    df_postes['Date'] = pd.to_datetime(df_postes['Date'])

    df_postes['Date'] = df_postes['Date'].dt.strftime('%Y%m%d')
    new_order = ['Date','Foyer', 'Poste','poids','Heure_début','Heure_Fin','Num_poste','Chaîne']
    df_postes=df_postes[new_order]
    df_postes=df_postes.to_dict('records')
    return df_postes

########################################################################### 4 CHAINES ############################################################################
########################################################################## Foyers entre dates ####################################################################
def get_foyers_pds_4chaines_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')

    infosFoyer = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/4_Chaines', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] == "00":
                    ligne = ligne.strip()
                    foyer_infos = {
                        "Date": date.strftime('%Y-%m-%d'),
                        "Type": "H",
                        "N_invité": 0,
                        "sexe": "0",
                        "Tranche_age": "0",
                        "Heure_début": 0,
                        "Heure_Fin": 0,
                        "Chaîne": 0,
                        "Indiv": ligne[1:7],
                        "poids": ligne[8:12]
                    }
                    infosFoyer.append(foyer_infos)

    df_foyers = pd.DataFrame(infosFoyer)


    df_foyers['Date'] = pd.to_datetime(df_foyers['Date'])

    df_foyers['Date'] = df_foyers['Date'].dt.strftime('%Y%m%d')

    

    df_foyers = df_foyers[['Date', 'Indiv', 'Type', 'N_invité', 'sexe', 'Tranche_age', 'poids', 'Heure_début', 'Heure_Fin', 'Chaîne']]

    df_foyers.rename(columns={'poids': 'poids', 'Indiv': 'Indiv'}, inplace=True)
    df_foyers=df_foyers.to_dict("records")
    return df_foyers




####################################################################### Individus entre dates #########################################################################""
def individus_pds_4chaines_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosIndividus = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/4_Chaines', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] in ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]:
                    contenu_ligne = ligne[13:]
                    contenu_ligne=contenu_ligne.split(',')[:-1]
                    if len(contenu_ligne)!= 0 :
                        for i in contenu_ligne:
                            ligne_infos = {}  
                            ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                            ligne_infos["Type"]="I"
                            ligne_infos["N_invité"]=0
                            ligne_infos["sexe"]="0"
                            ligne_infos["Tranche_age"]="0"
                            ligne_infos["Indiv"] = ligne[1:7]
                            ligne_infos["poids"] = ligne[8:12]
                            ligne_infos["Heure_début"] = int(i[1:6])
                            ligne_infos["Heure_Fin"] = int(i[6:11])
                            ligne_infos["Chaîne"] = i[12:15]
                            infosIndividus.append(ligne_infos)
                    else:
                        ligne_infos = {}  
                        ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                        ligne_infos["Type"]="I"
                        ligne_infos["N_invité"]=0
                        ligne_infos["sexe"]="0"
                        ligne_infos["Tranche_age"]="0"
                        ligne_infos["Indiv"] = ligne[1:7]
                        ligne_infos["poids"] = ligne[8:12]
                        ligne_infos["Heure_début"] = 0
                        ligne_infos["Heure_Fin"] = 0
                        ligne_infos["Chaîne"] = 0
                        infosIndividus.append(ligne_infos)               
    df_individus = pd.DataFrame(infosIndividus)
    df_individus['Date'] = pd.to_datetime(df_individus['Date'])

    df_individus['Date'] = df_individus['Date'].dt.strftime('%Y%m%d')

    new_order = ['Date', 'Indiv', 'Type','N_invité','sexe','Tranche_age','poids','Heure_début','Heure_Fin','Chaîne']
    #df_individus['Date'] = pd.to_datetime(df_individus['Date'])
    df_individus=df_individus[new_order]
    df_individus=df_individus.to_dict("records")
    return df_individus

########################################################################## Invites entre dates #####################################################################
def invites_pds_4chaines_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosInvites = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/4_Chaines', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue
        foyers_invites = {}
        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] == '32':
                    foyer_id = ligne[1:5]
                    sessions = ligne[17:]
                    sessions = sessions.split(',')[:-1]
                    if foyer_id in foyers_invites:
                        id_inv = foyers_invites[foyer_id] + 1
                        foyers_invites[foyer_id] = id_inv
                    else:
                        id_inv = 1
                        foyers_invites[foyer_id] = id_inv
                    if len(sessions)!= 0 :
                        for i in sessions:
                            invites_infos = {'N_invité': id_inv}
                            invites_infos["Date"]=date
                            invites_infos["Type"]="G"
                            invites_infos["Indiv"] = ligne[1:7]
                            invites_infos["poids"] = ligne[8:12]
                            invites_infos["sexe"] = ligne[13:14]
                            invites_infos["Tranche_age"] = ligne[15:16]
                            invites_infos["Heure_début"] = int(i[1:6])
                            invites_infos["Heure_Fin"] = int(i[6:11])
                            invites_infos["Chaîne"] = i[12:15]
                            infosInvites.append(invites_infos)
                    else:
                            invites_infos = {'N_invité': id_inv}
                            invites_infos["Date"]=date
                            invites_infos["Type"]="G"
                            invites_infos["Indiv"] = ligne[1:7]
                            invites_infos["poids"] = ligne[8:12]
                            invites_infos["sexe"] = ligne[13:14]
                            invites_infos["Tranche_age"] = ligne[15:16]
                            invites_infos["Heure_début"] = 0
                            invites_infos["Heure_Fin"] = 0
                            invites_infos["Chaîne"] = 0
                            infosInvites.append(invites_infos)
                        
    df_invites = pd.DataFrame(infosInvites)
    df_invites['Date'] = pd.to_datetime(df_invites['Date'])

    df_invites['Date'] = df_invites['Date'].dt.strftime('%Y%m%d')
    new_order = ['Date', 'Indiv', 'Type','N_invité','sexe','Tranche_age','poids','Heure_début','Heure_Fin','Chaîne']
    #df_invites['Date'] = pd.to_datetime(df_invites['Date'])
    df_invites=df_invites[new_order]
    df_invites=df_invites.to_dict('records')
    return df_invites


########################################################################## Postes entre dates #########################################################################

def postes_pds_4chaines_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosPostes = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/4_Chaines', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] in ["61", "62"]:
                    contenu_ligne = ligne[13:]
                    contenu_ligne=contenu_ligne.split(',')[:-1]
                    if len(contenu_ligne)!= 0 :
                        for i in contenu_ligne:
                            ligne_infos = {}  
                            ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                            ligne_infos["Poste"]=ligne[5:7]
                            ligne_infos["Foyer"] = ligne[1:5]
                            ligne_infos["poids"] = ligne[8:12]
                            ligne_infos["Heure_début"] = int(i[1:6])
                            ligne_infos["Heure_Fin"] = int(i[6:11])
                            ligne_infos["Num_poste"] = int(i[11:12])
                            ligne_infos["Chaîne"] = i[12:15]
                            infosPostes.append(ligne_infos)
                    else:
                        ligne_infos = {}  
                        ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                        ligne_infos["Poste"]=ligne[5:7]
                        ligne_infos["Foyer"] = ligne[1:5]
                        ligne_infos["poids"] = ligne[8:12]
                        ligne_infos["Heure_début"] = 0
                        ligne_infos["Heure_Fin"] = 0
                        ligne_infos["Num_poste"] = 0
                        ligne_infos["Chaîne"] = 0
                        infosPostes.append(ligne_infos)               
    df_postes = pd.DataFrame(infosPostes)
    df_postes['Date'] = pd.to_datetime(df_postes['Date'])

    df_postes['Date'] = df_postes['Date'].dt.strftime('%Y%m%d')
    new_order = ['Date','Foyer', 'Poste','poids','Heure_début','Heure_Fin','Num_poste','Chaîne']
    #df_postes['Date'] = pd.to_datetime(df_postes['Date'])

    df_postes=df_postes[new_order]
    df_postes=df_postes.to_dict('records')
    return df_postes


################################################################################## FP #####################################################################################
####################################################################### Foyers entre dates ################################################################################
def get_foyers_pds_FP_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')

    infosFoyer = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/FP', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] == "00":
                    ligne = ligne.strip()
                    foyer_infos = {
                        "Date": date.strftime('%Y-%m-%d'),
                        "Type": "H",
                        "N_invité": 0,
                        "sexe": "0",
                        "Tranche_age": "0",
                        "Heure_début": 0,
                        "Heure_Fin": 0,
                        "Chaîne": 0,
                        "Indiv": ligne[1:7],
                        "poids": ligne[8:12]
                    }
                    infosFoyer.append(foyer_infos)

    df_foyers = pd.DataFrame(infosFoyer)
    df_foyers['Date'] = pd.to_datetime(df_foyers['Date'])

    df_foyers['Date'] = df_foyers['Date'].dt.strftime('%Y%m%d')
    df_foyers = df_foyers[['Date', 'Indiv', 'Type', 'N_invité', 'sexe', 'Tranche_age', 'poids', 'Heure_début', 'Heure_Fin', 'Chaîne']]
   # df_foyers['Date'] = pd.to_datetime(df_foyers['Date'])

    df_foyers.rename(columns={'poids': 'poids', 'Indiv': 'Indiv'}, inplace=True)
    df_foyers=df_foyers.to_dict("records")
    return df_foyers


################################################################# Individus entre dates #################################################################################
def individus_pds_FP_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosIndividus = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/FP', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] in ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]:
                    contenu_ligne = ligne[13:]
                    contenu_ligne=contenu_ligne.split(',')[:-1]
                    if len(contenu_ligne)!= 0 :
                        for i in contenu_ligne:
                            ligne_infos = {}  
                            ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                            ligne_infos["Type"]="I"
                            ligne_infos["N_invité"]=0
                            ligne_infos["sexe"]="0"
                            ligne_infos["Tranche_age"]="0"
                            ligne_infos["Indiv"] = ligne[1:7]
                            ligne_infos["poids"] = ligne[8:12]
                            ligne_infos["Heure_début"] = int(i[1:6])
                            ligne_infos["Heure_Fin"] = int(i[6:11])
                            ligne_infos["Chaîne"] = i[12:15]
                            infosIndividus.append(ligne_infos)
                    else:
                        ligne_infos = {}  
                        ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                        ligne_infos["Type"]="I"
                        ligne_infos["N_invité"]=0
                        ligne_infos["sexe"]="0"
                        ligne_infos["Tranche_age"]="0"
                        ligne_infos["Indiv"] = ligne[1:7]
                        ligne_infos["poids"] = ligne[8:12]
                        ligne_infos["Heure_début"] = 0
                        ligne_infos["Heure_Fin"] = 0
                        ligne_infos["Chaîne"] = 0
                        infosIndividus.append(ligne_infos)               
    df_individus = pd.DataFrame(infosIndividus)
    df_individus['Date'] = pd.to_datetime(df_individus['Date'])

    df_individus['Date'] = df_individus['Date'].dt.strftime('%Y%m%d')
    new_order = ['Date', 'Indiv', 'Type','N_invité','sexe','Tranche_age','poids','Heure_début','Heure_Fin','Chaîne']
    #df_individus['Date'] = pd.to_datetime(df_individus['Date'])
    df_individus=df_individus[new_order]
    df_individus=df_individus.to_dict("records")
    return df_individus


################################################################################### Invites entre dates #####################################################################
def invites_pds_FP_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosInvites = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/FP', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue
        foyers_invites = {}
        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] == '32':
                    foyer_id = ligne[1:5]
                    sessions = ligne[17:]
                    sessions = sessions.split(',')[:-1]
                    if foyer_id in foyers_invites:
                        id_inv = foyers_invites[foyer_id] + 1
                        foyers_invites[foyer_id] = id_inv
                    else:
                        id_inv = 1
                        foyers_invites[foyer_id] = id_inv
                    if len(sessions)!= 0 :
                        for i in sessions:
                            invites_infos = {'N_invité': id_inv}
                            invites_infos["Date"]=date
                            invites_infos["Type"]="G"
                            invites_infos["Indiv"] = ligne[1:7]
                            invites_infos["poids"] = ligne[8:12]
                            invites_infos["sexe"] = ligne[13:14]
                            invites_infos["Tranche_age"] = ligne[15:16]
                            invites_infos["Heure_début"] = int(i[1:6])
                            invites_infos["Heure_Fin"] = int(i[6:11])
                            invites_infos["Chaîne"] = i[12:15]
                            infosInvites.append(invites_infos)
                    else:
                            invites_infos = {'N_invité': id_inv}
                            invites_infos["Date"]=date
                            invites_infos["Type"]="G"
                            invites_infos["Indiv"] = ligne[1:7]
                            invites_infos["poids"] = ligne[8:12]
                            invites_infos["sexe"] = ligne[13:14]
                            invites_infos["Tranche_age"] = ligne[15:16]
                            invites_infos["Heure_début"] = 0
                            invites_infos["Heure_Fin"] = 0
                            invites_infos["Chaîne"] = 0
                            infosInvites.append(invites_infos)
                        
    df_invites = pd.DataFrame(infosInvites)
    df_invites['Date'] = pd.to_datetime(df_invites['Date'])

    df_invites['Date'] = df_invites['Date'].dt.strftime('%Y%m%d')
    new_order = ['Date', 'Indiv', 'Type','N_invité','sexe','Tranche_age','poids','Heure_début','Heure_Fin','Chaîne']
    #df_invites['Date'] = pd.to_datetime(df_invites['Date'])
    df_invites=df_invites[new_order]
    df_invites=df_invites.to_dict('records')
    return df_invites


#################################################################################### Postes entre dates #####################################################################

def postes_pds_FP_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosPostes = []
    for date in dates:
        chemin_fichier = os.path.join('PAN_PDS_API/input/FP', f'{date.strftime("%Y%m%d")}.pds')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[5:7] in ["61", "62"]:
                    contenu_ligne = ligne[13:]
                    contenu_ligne=contenu_ligne.split(',')[:-1]
                    if len(contenu_ligne)!= 0 :
                        for i in contenu_ligne:
                            ligne_infos = {}  
                            ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                            ligne_infos["Poste"]=ligne[5:7]
                            ligne_infos["Foyer"] = ligne[1:5]
                            ligne_infos["poids"] = ligne[8:12]
                            ligne_infos["Heure_début"] = int(i[1:6])
                            ligne_infos["Heure_Fin"] = int(i[6:11])
                            ligne_infos["Num_poste"] = int(i[11:12])
                            ligne_infos["Chaîne"] = i[12:15]
                            infosPostes.append(ligne_infos)
                    else:
                        ligne_infos = {}  
                        ligne_infos["Date"]=date.strftime('%Y-%m-%d')
                        ligne_infos["Poste"]=ligne[5:7]
                        ligne_infos["Foyer"] = ligne[1:5]
                        ligne_infos["poids"] = ligne[8:12]
                        ligne_infos["Heure_début"] = 0
                        ligne_infos["Heure_Fin"] = 0
                        ligne_infos["Num_poste"] = 0
                        ligne_infos["Chaîne"] = 0
                        infosPostes.append(ligne_infos)               
    df_postes = pd.DataFrame(infosPostes)
    df_postes['Date'] = pd.to_datetime(df_postes['Date'])

    df_postes['Date'] = df_postes['Date'].dt.strftime('%Y%m%d')
    new_order = ['Date','Foyer', 'Poste','poids','Heure_début','Heure_Fin','Num_poste','Chaîne']
    #df_postes['Date'] = pd.to_datetime(df_postes['Date'])
    df_postes=df_postes[new_order]
    df_postes=df_postes.to_dict('records')
    return df_postes





########################################################################### Get ALL Individus ######################################################################
def get_All_Indiv(date_debut, date_fin):
    individus=pd.DataFrame(individus_pds_entre_dates(date_debut, date_fin))
    invites=pd.DataFrame(invites_pds_entre_dates(date_debut, date_fin))
    result = pd.concat([individus, invites], ignore_index=True)
    result=result.sort_values(['Date','Indiv'])
    result=result.to_dict('records')
    return result

########################################################################### Get Foyers et Individus ######################################################################
def get_Foyers_Indiv(date_debut, date_fin):
    individus=pd.DataFrame(individus_pds_entre_dates(date_debut, date_fin))
    foyers=pd.DataFrame(get_foyers_pds_entre_dates(date_debut, date_fin))
    result = pd.concat([foyers, individus], ignore_index=True)
    result=result.sort_values(['Date','Indiv'])
    result=result.to_dict('records')
    return result

####################################### Cumul sessions des postes ###############################################################

def get_cumul_moyenne_sessions_postes(date_debut,date_fin):
    Audience=[]
    df=pd.DataFrame(postes_pds_entre_dates(date_debut,date_fin))
    postes_pds=filtrer_dataframe_advanced(df,Heure_début={'op':'ne','value':0},Heure_Fin={'op':'ne','value':0})
    for foyer,groupe in postes_pds.groupby(['Date','Foyer','Poste']):
        infos_sessions={'date':foyer[0],'foyer':foyer[1],'poste':foyer[2],'Count_sessions':len(groupe),'Cumul_sessions':sum(groupe['Heure_Fin']-groupe['Heure_début']),'Moyenne_sessions':int(sum(groupe['Heure_Fin']-groupe['Heure_début'])/len(groupe))}
        Audience.append(infos_sessions)
    cumul_sessions_df=pd.DataFrame(Audience)  
    cumul_sessions_df=cumul_sessions_df.to_dict('records')
    return cumul_sessions_df

####################################### Pourcentage d'audience des postes par rapport à l'audience foyer globale ###############################################################
def pourcentage_audience_postes(date_debut, date_fin):
    Audience = []
    Total = []
    df=pd.DataFrame(postes_pds_entre_dates(date_debut, date_fin))
    postes_pds = filtrer_dataframe_advanced(df, Heure_début={'op': 'ne', 'value': 0}, Heure_Fin={'op': 'ne', 'value': 0})
    
    for foyer,groupe in postes_pds.groupby(['Date','Foyer','Poste']):
        infos_sessions = {'Date':foyer[0],'Foyer': foyer[1], 'poste': foyer[2], 'Count_sessions': len(groupe), 'Cumul_sessions': sum(groupe['Heure_Fin'] - groupe['Heure_début']), 'Moyenne_sessions': int(sum(groupe['Heure_Fin'] - groupe['Heure_début']) / len(groupe))}
        Audience.append(infos_sessions)
    Audience=pd.DataFrame(Audience)
    # Ajouter une ligne pour le poste "ALL" pour chaque foyer
    for foyer, groupe in Audience.groupby(['Date','Foyer']):
        total_sessions = sum(groupe['Cumul_sessions'])
        total_count = sum(groupe['Count_sessions'])
        moyenne_totale = int(total_sessions / total_count) if total_count > 0 else 0
        infos_sessions_all = {'Date':foyer[0],'Foyer': foyer[1], 'poste': 'ALL', 'Count_sessions': total_count, 'Cumul_sessions': total_sessions, 'Moyenne_sessions': moyenne_totale}
        Total.append(infos_sessions_all)
    Total=pd.DataFrame(Total)
    cumul_sessions_df = pd.concat([Audience,Total], axis=0)
    cumul_sessions_df = cumul_sessions_df.sort_values(by=['Date','Foyer','poste'])
    
    total_sessions_all = cumul_sessions_df[cumul_sessions_df['poste'] == 'ALL'].groupby(['Date', 'Foyer'])['Cumul_sessions'].sum().reset_index()
    total_sessions_all.rename(columns={'Cumul_sessions': 'Cumul_sessions_global'}, inplace=True)

    # Fusionner les informations du cumul 'ALL' avec le DataFrame d'origine
    cumul_sessions_df = cumul_sessions_df.merge(total_sessions_all, on=['Date', 'Foyer'], how='left')

    # Calculer le pourcentage de cumul_sessions par rapport au cumul de foyer (poste='ALL')
    cumul_sessions_df['pourcentage'] = round((cumul_sessions_df['Cumul_sessions'] / cumul_sessions_df['Cumul_sessions_global']) * 100,2) 
    cumul_sessions_df['pourcentage'] = cumul_sessions_df['pourcentage'].astype(str) + '%'
    #cumul_sessions_df = filtrer_dataframe_advanced(cumul_sessions_df, poste={'op': 'ne', 'value': 'ALL'})
    cumul_sessions_df=cumul_sessions_df.to_dict('records')

    return cumul_sessions_df


####################################### Cumul audience foyer ###############################################################

def cumul_audience_foyer(date_debut, date_fin):
    postes=pd.DataFrame(pourcentage_audience_postes(date_debut, date_fin))
    df=filtrer_dataframe_advanced(postes,poste=['ALL'])
    df = df.drop(['poste', 'Cumul_sessions_global','pourcentage'], axis=1)
    df = df.to_dict('records')
    return df
####################################### Evolution audience par date ###############################################################


def evolution_audience_par_date(date_debut, date_fin):
    df=pd.DataFrame(cumul_audience_foyer(date_debut,date_fin))
    moyenne_cumul_sessions = df.groupby('Date')['Cumul_sessions'].mean()
    moyenne_cumul_sessions_df = pd.DataFrame({'Date': moyenne_cumul_sessions.index, 'Moyenne_cumul_sessions': moyenne_cumul_sessions.values})
    moyenne_cumul_sessions_df=moyenne_cumul_sessions_df.to_dict('records')
    return moyenne_cumul_sessions_df


################################################ Anomalie RÉPONDANT No Tickets   #######################################################################


def get_anomalie_indiv_repond_no_tickets(date_debut,date_fin):
    df=pd.DataFrame(individus_pds_entre_dates(date_debut,date_fin))
    ano_repond_no_tickets=filtrer_dataframe_advanced(df,poids={'op': 'ne', 'value': 0},Heure_début={'op': 'eq', 'value': 0},Heure_Fin={'op': 'eq', 'value': 0})
    ano_repond_no_tickets["ID_Foyer"] = ano_repond_no_tickets["Indiv"].apply(get_foyer)
    ano_repond_no_tickets=ano_repond_no_tickets.to_dict('records')
    print(ano_repond_no_tickets)
    return ano_repond_no_tickets  



############################################################ Cumul et Moyenne des sessions des individus  #######################################################################

def get_cumul_moyenne_sessions(date_debut,date_fin):
    df=pd.DataFrame(individus_pds_entre_dates(date_debut,date_fin))
    individus_pds=filtrer_dataframe_advanced(df,Heure_début={'op':'ne','value':0},Heure_Fin={'op':'ne','value':0},poids={'op':'ne','value':'0000'})
    individus_pds["ID_Foyer"] = individus_pds["Indiv"].apply(get_foyer)
    Audience=[]
    for foyer,groupe in individus_pds.groupby(['Date','ID_Foyer','Indiv']):
        infos_sessions={'date':foyer[0],'foyer':foyer[1],'individu':foyer[2],'Count_sessions':len(groupe),'Cumul_sessions':sum(groupe['Heure_Fin']-groupe['Heure_début']),'Moyenne_sessions':int(sum(groupe['Heure_Fin']-groupe['Heure_début'])/len(groupe))}
        Audience.append(infos_sessions)
    cumul_sessions_df=pd.DataFrame(Audience) 
    cumul_sessions_df=cumul_sessions_df.sort_values(['date','foyer'],ascending=False)

    cumul_sessions_df=cumul_sessions_df.to_dict('records') 
    return cumul_sessions_df

############################################################ Pourcentage audience des touches #######################################################################

def get_pourcentage_audience_touches(date_debut, date_fin):
    Total = []
    df = pd.DataFrame(get_cumul_moyenne_sessions(date_debut, date_fin))
    df["Touche"] = df["individu"].apply(get_touche)    
    for foyer, groupe in df.groupby(['date','foyer']):
        total_sessions = sum(groupe['Cumul_sessions'])
        total_count = sum(groupe['Count_sessions'])
        moyenne_totale = int(total_sessions / total_count) if total_count > 0 else 0
        infos_sessions_all = {'date':foyer[0],'foyer': foyer[1], 'individu': 'ALL', 'Count_sessions': total_count, 'Cumul_sessions': total_sessions, 'Moyenne_sessions': moyenne_totale}
        Total.append(infos_sessions_all)
    Total=pd.DataFrame(Total)
    cumul_sessions_df = pd.concat([df,Total], axis=0)
    cumul_sessions_df = cumul_sessions_df.sort_values(by=['date','foyer','individu'])
    total_sessions_all = cumul_sessions_df[cumul_sessions_df['individu'] == 'ALL'].groupby(['date', 'foyer'])['Cumul_sessions'].sum().reset_index()
    total_sessions_all.rename(columns={'Cumul_sessions': 'Cumul_sessions_global'}, inplace=True)
    cumul_sessions_df = cumul_sessions_df.merge(total_sessions_all, on=['date', 'foyer'], how='left')
    cumul_sessions_df['pourcentage'] = round((cumul_sessions_df['Cumul_sessions'] / cumul_sessions_df['Cumul_sessions_global']) * 100,1) 
    #cumul_sessions_df['pourcentage'] = cumul_sessions_df['pourcentage'].astype(str) + '%'
    cumul_sessions_df=filtrer_dataframe_advanced(cumul_sessions_df, individu={'op': 'ne', 'value': 'ALL'})
    cumul_sessions_df=cumul_sessions_df.drop(['Cumul_sessions_global'], axis=1)
    cumul_sessions_df.rename(columns={'date':'Date','foyer': 'Foyer', 'individu': 'Individu'}, inplace=True)
    cumul_sessions_df=cumul_sessions_df.to_dict('records') 
    return cumul_sessions_df


################################################ Foyers déclarant Touche 1 #######################################################################


def get_foyers_declarant_touche_1(date_debut, date_fin):
    df = pd.DataFrame(get_pourcentage_audience_touches(date_debut, date_fin))
    df = filtrer_dataframe_advanced(df, Touche={'op': 'eq', 'value': '1'})
    df['total_membre'] = df.apply(get_total_membre, axis=1)
    df['f_num_con'] = df.apply(get_Num_Con, axis=1) 
    df['id_indiv'] = df.apply(get_id_indiv, axis=1)
    df=filtrer_dataframe_advanced(df, total_membre={'op': 'gt', 'value': 1},pourcentage={'op': 'eq', 'value': 100})
    df['pourcentage'] = df['pourcentage'].astype(str) + '%'
    df=df.drop(['Touche','Individu','Moyenne_sessions','Count_sessions'], axis=1)
    df=df.sort_values(['Cumul_sessions'],ascending=False)
    df.rename(columns={'total_membre':'Total Membre','id_indiv':'Individu','pourcentage':'Pourcentage Audience','Moyenne_sessions':'Moyenne Sessions','Count_sessions':'Count Sessions','Cumul_sessions':'Cumul Sessions'}, inplace=True)
    new_order = ['Date', 'Foyer','f_num_con','Individu','Cumul Sessions','Pourcentage Audience','Total Membre']
    df=df[new_order]

    df=df.to_dict('records')
    return df

################################################ Anomalie 24h  #######################################################################

def get_ano_24h_indiv(date_debut,date_fin):
    df=pd.DataFrame(get_cumul_moyenne_sessions(date_debut,date_fin))
    df_anomalie_24h=filtrer_dataframe_advanced(df,Cumul_sessions=[86400])
    df_anomalie_24h=df_anomalie_24h.to_dict('records')
    return df_anomalie_24h


################################################ Anomalie Meme audience  #######################################################################

def get_ano_meme_aud_indiv(date_debut,date_fin):
    df=pd.DataFrame(get_cumul_moyenne_sessions(date_debut,date_fin))
    grouped = df.groupby(['foyer', 'Moyenne_sessions'])
    same_avg_sessions_list = []
    for (foyer, moyenne_sessions), group_data in grouped:
        if len(group_data) > 1:
            same_avg_sessions_list.append(group_data)
    ano_meme_aud_df = pd.concat(same_avg_sessions_list)
    ano_meme_aud_df=ano_meme_aud_df.sort_values(['Cumul_sessions','foyer'],ascending=False)
    ano_meme_aud_df=ano_meme_aud_df.to_dict('records')
    return ano_meme_aud_df


################################################ Anomalie Meme début de déclaration  #######################################################################

def get_ano_meme_debut_declaration(date_debut,date_fin):
    df_individus=pd.DataFrame(individus_pds_entre_dates(date_debut,date_fin))
    individus_pds=filtrer_dataframe_advanced(df_individus,Heure_début={'op':'ne','value':0},Heure_Fin={'op':'ne','value':0})
    individus_pds["ID_Foyer"] = individus_pds["Indiv"].apply(get_foyer)
    grouped = individus_pds.groupby(['Date','ID_Foyer','Heure_début'])
    #display(grouped)
    same_debut_sessions_list = []
    for (Date,ID_Foyer,Heure_début), group_data in grouped:
        #print(group_data)
        if len(group_data) > 1:
            
            #print(group_data)
            same_debut_sessions_list.append(group_data)
    df = pd.concat(same_debut_sessions_list)
    #df=df.drop(['Type','N_invité','sexe','Tranche_age'])
    new_order = ['Date', 'ID_Foyer','Indiv','Heure_début','Heure_Fin','Chaîne']
    df=df[new_order]
    df.rename(columns={'ID_Foyer': 'Foyer', 'Indiv': 'Individu'}, inplace=True)
    df=df.to_dict('records')

    return df


############################################################# Toutes les anomalies des individus  #################################################################

def toutes_anomalies_indiv(date_debut,date_fin):
    get_ano_24h_indiv_df=pd.DataFrame(get_ano_24h_indiv(date_debut,date_fin))
    get_cumul_moyenne_sessions_df=pd.DataFrame(get_cumul_moyenne_sessions(date_debut,date_fin))
    get_ano_meme_aud_indiv_df=pd.DataFrame(get_ano_meme_aud_indiv(date_debut,date_fin))

    merged_df = get_cumul_moyenne_sessions_df.merge(get_ano_24h_indiv_df[['foyer', 'individu']], on=['foyer', 'individu'], how='left', indicator=True, suffixes=('', '_y'))
    merged_df['ano_24h'] = merged_df['_merge'] == 'both'
    merged_df.drop('_merge', axis=1, inplace=True)

    

    final_merged_df = merged_df.merge(get_ano_meme_aud_indiv_df[['foyer', 'individu']],  on=['foyer', 'individu'], how='left', indicator=True, suffixes=('', '_y'))
    final_merged_df['ano_meme_aud'] = final_merged_df['_merge'] == 'both'
    final_merged_df.drop('_merge', axis=1, inplace=True)
    final_merged_df['ano_24h'] = final_merged_df['ano_24h'].replace({True: 'Oui', False: 'Non'})
    final_merged_df['ano_meme_aud'] = final_merged_df['ano_meme_aud'].replace({True: 'Oui', False: 'Non'})
    final_merged_df=final_merged_df.to_dict('records')
    return final_merged_df


######################################################################################### Infos FP ###########################################################
def get_infos_FP(date_debut, date_fin):
    individus=pd.DataFrame(individus_pds_FP_entre_dates(date_debut, date_fin))
    invites=pd.DataFrame(invites_pds_FP_entre_dates(date_debut, date_fin))
    foyers=pd.DataFrame(get_foyers_pds_FP_entre_dates(date_debut, date_fin))
    result = pd.concat([foyers,individus,invites], ignore_index=True)
    result['Chaîne'] = result['Chaîne'].replace('047', '237')
    #result['Date'] = pd.to_datetime(result['Date'])
    result=result.sort_values(['Date','Indiv'])
    result=result.to_dict('records')
    return result


################################################################## Infos 4 Chaines ##############################################################################
def get_infos_4Chaines(date_debut, date_fin):
    individus=pd.DataFrame(individus_pds_4chaines_entre_dates(date_debut, date_fin))
    invites=pd.DataFrame(invites_pds_4chaines_entre_dates(date_debut, date_fin))
    foyers=pd.DataFrame(get_foyers_pds_4chaines_entre_dates(date_debut, date_fin))
    result = pd.concat([foyers,individus,invites], ignore_index=True)
    #result['Chaîne'] = result['Chaîne'].replace('047', '237')
    Chaines_7Chaines = ['098','099','095' ,0]
    result.loc[~result['Chaîne'].isin(Chaines_7Chaines), 'Chaîne'] = '237'
    #result['Date'] = pd.to_datetime(result['Date'])
    result=result.sort_values(['Date','Indiv'])
    result=result.to_dict('records')
    return result
########################################################################################################################

################################################################## Infos 7 Chaines ##############################################################################
