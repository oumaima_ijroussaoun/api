def include_route(app):
    
    app.include_router(pds.routePDS)
    app.include_router(aud.routeAUD)
    app.include_router(pan.routePAN)
    app.include_router(triple_S.routeTriple_S)
