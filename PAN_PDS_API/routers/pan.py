from fastapi import APIRouter, Depends,HTTPException,status
from datetime import date, datetime, timedelta
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from typing import List
from crud import pan




routePAN = APIRouter(prefix="/API",tags=["API"])


############### Get Foyers Infos #################
@routePAN.get("/get/FoyersInfos/")
def get_foyers_pan(date_debut:date,date_fin:date):
    
    
    return pan.get_foyers_pan_entre_dates(date_debut,date_fin)

############### Get Individus Infos #################
@routePAN.get("/get/IndividusInfos/")
def get_individus_pan(date_debut:date,date_fin:date):
    
    
    return pan.individus_pan_entre_dates(date_debut,date_fin)


############## Get Foyers Infos from "house_hold_triple_s" view #################
@routePAN.get("/get/House_Hold_Triple_s/")
def get_house_hold_triple_s():
    
    
    return pan.get_house_hold_triple_s()


############### Get Oublic foyer from "foyer" table #################
@routePAN.get("/get/FoyersInfos_Public_Foyer/")
def get_public_foyer():
    
    
    return pan.get_public_foyer()



@routePAN.get("/get/PAN_CSV/")
def create_pan_csv(date_debut:date,date_fin:date):
    
    
    return pan.create_pan_csv(date_debut,date_fin)


@routePAN.get("/get/foyers_triple_s/")
def get_foyers_triple_s():
    
    
    return pan.get_foyers_triple_s()
