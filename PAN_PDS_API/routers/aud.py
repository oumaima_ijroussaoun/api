from fastapi import APIRouter, Depends,HTTPException,status
from datetime import date, datetime, timedelta
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from typing import List
from crud import aud




routeAUD = APIRouter(prefix="/API",tags=["API"])


############### Get Foyers Alarmes #################
@routeAUD.get("/get/FoyersAlarmes/")
def get_foyer_alarmes(date_debut:date,date_fin:date):
    
    
    return aud.get_foyer_alarmes(date_debut,date_fin)

############### Get Meter Alarmes #################
@routeAUD.get("/get/MetersAlarmes/")
def get_meter_alarmes(date_debut:date,date_fin:date):
    
    
    return aud.get_meter_alarmes(date_debut,date_fin)

############### Get Audience #################
@routeAUD.get("/get/Audience_XML/")
def aud_xml(date_debut:date,param:str):
    
    
    return aud.aud_xml(date_debut,param)
